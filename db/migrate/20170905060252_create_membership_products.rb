class CreateMembershipProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :membership_products do |t|
      t.integer :product_id
      t.integer :membership_id
      t.string :status
      t.integer :recurring_product_id

      t.timestamps
    end
  end
end
