class CreateMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :memberships do |t|
      t.string :status #it is int in leagacy
      t.datetime :start_date
      t.string :numeber_of_fail_payments
      t.string :state
      #legacy fields
	  t.integer :subscribed_provider_id
	  t.string :subscribed_list_id
	  t.string :origin_affiliate_id
	  t.string :origin_subaffiliate_id
	  t.datetime :became_active
	  t.datetime :welcome_email_sent
	  t.datetime :last_login_date
	  t.datetime :status_updated
	  t.datetime :cancellation_date
	  t.datetime :expiration_date
      t.timestamps
    end
  end
end





