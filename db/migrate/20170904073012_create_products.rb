class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :reference_key
      t.integer :status
      t.string :name
      t.string :sku
      t.text :description
      t.float :price
      t.string :currency
      t.integer :is_shippable
      t.integer :has_trial
      t.string :trial_frequency #enum('months', 'days', 'weeks', 'years') default 'months' null
      t.integer :rebill_period 
      t.integer :rebill_frequency #enum('months', 'days', 'weeks', 'years') default 'months' null
      t.float :trial_amount
      t.integer :trial_duration
      t.integer :do_limit_trial
      t.integer :limit_trial_alt_product_id 
      t.integer :do_limit_payments
      t.integer :number_of_payments
      t.text :purchase_confirmation_message
      t.integer :commission_profile_id, default: '-1'
   	  #exrta added columns
      t.string :product_type
      t.timestamps
    end
  end
end
