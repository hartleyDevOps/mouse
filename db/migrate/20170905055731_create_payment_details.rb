class CreatePaymentDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_details do |t|
      t.string :billing_address1
	  t.string :billing_address2
	  t.string :billing_city 
	  t.string :billing_state
	  t.string :billing_province
	  t.string :billing_postal_code
	  t.string :billing_country
	  t.string :shipping_address1
	  t.string :shipping_address2
	  t.string :shipping_city
	  t.string :shipping_state
	  t.string :shipping_province
	  t.string :shipping_postal_code
	  t.string :shipping_country
      t.timestamps
    end
  end
end
