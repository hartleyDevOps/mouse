class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.string :message
      t.float :basic_amount
      t.float :difference
      t.float :charged_amount
      t.string :transaction_type1
      t.string :transaction_type2
      t.string :status
      t.string :fingerprint
      t.integer :payment_number
      t.integer :attempt

      t.timestamps
    end
  end
end
