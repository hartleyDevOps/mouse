# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170905060519) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bundle_products", force: :cascade do |t|
    t.integer "bundle_id"
    t.integer "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bundles", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "status"
    t.integer "dflt_membership_id", default: 0
    t.integer "expire_amount"
    t.string "expire_period"
    t.integer "expires"
    t.string "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "membership_products", force: :cascade do |t|
    t.integer "product_id"
    t.integer "membership_id"
    t.string "status"
    t.integer "recurring_product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", force: :cascade do |t|
    t.string "status"
    t.datetime "start_date"
    t.string "numeber_of_fail_payments"
    t.string "state"
    t.integer "subscribed_provider_id"
    t.string "subscribed_list_id"
    t.string "origin_affiliate_id"
    t.string "origin_subaffiliate_id"
    t.datetime "became_active"
    t.datetime "welcome_email_sent"
    t.datetime "last_login_date"
    t.datetime "status_updated"
    t.datetime "cancellation_date"
    t.datetime "expiration_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_details", force: :cascade do |t|
    t.string "billing_address1"
    t.string "billing_address2"
    t.string "billing_city"
    t.string "billing_state"
    t.string "billing_province"
    t.string "billing_postal_code"
    t.string "billing_country"
    t.string "shipping_address1"
    t.string "shipping_address2"
    t.string "shipping_city"
    t.string "shipping_state"
    t.string "shipping_province"
    t.string "shipping_postal_code"
    t.string "shipping_country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.integer "limit_trial_alt_product_id"
    t.integer "do_limit_payments"
    t.integer "number_of_payments"
    t.text "purchase_confirmation_message"
    t.integer "commission_profile_id", default: -1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string "message"
    t.float "basic_amount"
    t.float "difference"
    t.float "charged_amount"
    t.string "transaction_type1"
    t.string "transaction_type2"
    t.string "status"
    t.string "fingerprint"
    t.integer "payment_number"
    t.integer "attempt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
